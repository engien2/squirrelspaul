
<!-- README.md is generated from README.Rmd. Please edit that file -->

# squirrelspaul

<!-- badges: start -->
<!-- badges: end -->

The goal of squirrelspaul is to analyze the squirrel data at Central
Parc

<img src="../data/squirrels_hex.png" width="10%" style="display: block; margin: auto 0 auto auto;" />

## Installation

You can install the development version of squirrelspaul like so:

``` r
remotes::install_local(path = "~/squirrels_0.0.0.9000.tar.gz")
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
get_message_fur_color("Cinnamon")
#> We will focus on Cinnamon squirrels !!!
check_primary_color_is_ok(c("Cinnamon"))
#> [1] TRUE
```
